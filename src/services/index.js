/* eslint-disable */
import store from '@/stores'
import request from './request'
import qs from 'qs'

export default {
    addTodo(todo) {

        request.post('/post', qs.stringify(todo), {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).then(res => {
            this.getTodoList()
        })
    },
    updateTodo(todo) {

        request.put('/' + todo._id, qs.stringify(todo)).then(res => {
            this.getTodoList()
        })
    },
    removeTodo(todo) {

        request.delete('/' + todo._id).then(res => {
            this.getTodoList()
        })
    },
    updateTodoList(_id, index) {

        request.put('/list/' + _id + '/' + index).then(res => {
            this.getTodoList()
        })
    },
    getTodoList() {
        return new Promise((resolve, reject) => {
            request.get('/').then(res => {
                store.commit('todoList', res.data)
                resolve(res.data)
            })
        })
    }
}