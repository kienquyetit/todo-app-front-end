import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    todoList: []
  },
  mutations: {
    todoList(state, todoList) {
      state.todoList = todoList
    }
  },
  getters: {
    todoList(state) {
      return state.todoList
    }
  }
})
